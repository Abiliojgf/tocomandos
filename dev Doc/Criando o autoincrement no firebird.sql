--Criando o autoincrement no firebird
--A criação precisa ser feita através de um generator e um trigger.
--Pode ser criado via script, comandos SQL ou por um programa de conexão à bases Firebird, como o ibconsole.
--Abaixo segue o processo para criação:

 PASSO I: generator
O generator é responsável por controlar o auto incremento.


CREATE GENERATOR nome_do_generator;

--PASSO II: trigger
--Use o seguinte código:
--Substitua os termos nome_da_trigger e nome_da_tabela pelos correspondentes em sua base de dados.


CREATE TRIGGER nome_da_trigger for nome_da_tabela
BEFORE INSERT position 0
AS
BEGIN
new.id = gen_id("nome_do_generator",1);
END;
